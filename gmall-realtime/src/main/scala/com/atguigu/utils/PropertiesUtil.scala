package com.atguigu.utils

import com.google.common.annotations.VisibleForTesting

import java.io.InputStreamReader
import java.util.Properties

object PropertiesUtil {

  // 读取配置文件
  def load(propertieName: String): Properties ={

    val properties = new Properties()
    properties.load(new InputStreamReader(Thread.currentThread().getContextClassLoader
                                                            .getResourceAsStream(propertieName), "UTF-8"))

    properties
  }

  // 配置文件读取测试
  def main(args: Array[String]): Unit ={
    val propertieName = "config.properties"

    val properties = load(propertieName)
    val str = properties.getProperty("kafka.broker.list")
    println(str)
  }

}
